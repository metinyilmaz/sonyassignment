// Image Manager
"use strict";

var ImageManager = function (id, data) {
    var $t = this;

    $t.id = id;
    $t.data = data;

    $t._init();
};

ImageManager.prototype = {
    version: '1.0.0',
    id: '',
    element: null,
    data: {},
    constructor: ImageManager,
    isInternal: false,
    _hotSpots: [],
    _popups: [],
    _imageFalmilyId: "",
    _init: function () {
        var $t = this;

        var element = $t.element = document.getElementById($t.id);
        if (!$t.element) {
            throw new Exception($t.id + " Element not found!");
        }

        if (!$t.data) {
            throw new Exception("Data not found!");
        }
        element.style.position = 'relative';
        element.innerHtml = '';
        $t._imageFalmilyId = $t.data.module.hotSpots.image.imageFamily.masterId.replace($t.data.module.hotSpots.image.imageFamily.type + ":", "");
        $t._createImage($t);
        $t._createHotSpots($t);
    },

    // All Event Handlers
    events: {
        // when windows is resized, this handler will trigger
        elementOnResize: function (event, $t) {
            var elementWidth = $t.element.clientWidth;
            $t._imageResize($t, elementWidth);
        },
    },

    // HotSpots Functions
    _createHotSpots: function ($t) {
        for (var i = 0; i < $t.data.module.hotSpots.popups.length; i++) {
            var o = $t.data.module.hotSpots.popups[i];

            var div = document.createElement("div");
            div.id = $t.id + "_hotspots_" + i;
            div.style.position = "absolute";
            div.style.top = o.config.coords[1];
            div.style.left = o.config.coords[0];
            div.style.lineHeight = "40px";
            div.style.cursor = "pointer";
            div.className = "hotspot";

            if (o.config.size === "large") {
                div.style.width = "40px";
                div.style.height = "40px";
                div.style["-webkit-border-radius"] = "20px";
                div.style["-moz-border-radius"] = "20px";
                div.style["border-radius"] = "20px";
                div.style.fontSize = "25px";
                div.style.fontWeight = "normal";
                //div.className += " hotspot-large"; 

            } else if (o.config.size === "medium") {
                div.style.width = "30px";
                div.style.height = "30px";
                div.style["-webkit-border-radius"] = "10px";
                div.style["-moz-border-radius"] = "10px";
                div.style["border-radius"] = "10px";

            } else if (o.config.size === "small") {
                div.style.width = "20px";
                div.style.height = "20px";
                div.style["-webkit-border-radius"] = "10px";
                div.style["-moz-border-radius"] = "10px";
                div.style["border-radius"] = "10px";
            } else {
                div.style.width = "40px";
                div.style.height = "40px";
                div.style["-webkit-border-radius"] = "10px";
                div.style["-moz-border-radius"] = "10px";
                div.style["border-radius"] = "10px";
            }

            div.innerHTML = "+";
            div["data-t"] = $t;
            div["data-hsid"] = i;
            div["data-direction"] = "off";
            div.addEventListener("click", function (event) {
                var $t = this["data-t"];
                var i = this["data-hsid"];
                var direction = this["data-direction"];

                if (direction === "off") {
                    this.style["-ms-transform"] = "rotate(-45deg)"; /* IE 9 */
                    this.style["-webkit-transform"] = "rotate(-45deg)"; /* Chrome, Safari, Opera */
                    this.style["transform"] = "rotate(-45deg)";
                    this["data-direction"] = "on";
                    
                    $t._popupShow($t, i);
                }
                else {
                    this.style["-ms-transform"] = "rotate(0deg)"; /* IE 9 */
                    this.style["-webkit-transform"] = "rotate(0deg)"; /* Chrome, Safari, Opera */
                    this.style["transform"] = "rotate(0deg)";
                    this["data-direction"] = "off";
                    $t._popupHide($t, i);                    
                }
            });
            $t.element.appendChild(div);
            $t._hotSpots[i] = div;


            var overlaydiv = document.getElementById("popupOverlay");
            if (!overlaydiv) {
                var overlaydiv = document.createElement("div");
                overlaydiv.id = "popupOverlay";
                overlaydiv.style.display = "none";
                document.body.appendChild(overlaydiv);
            }
            $t._overlaydiv = overlaydiv;

            var popupdiv = document.createElement("div");
            popupdiv.id = $t.id + "_popups_" + i;
            popupdiv.style.position = "absolute";
            popupdiv.style.display = "none";
            popupdiv.className = "popup-div";
            popupdiv.style.top = o.config.coords[1];
            popupdiv.style.left = o.config.coords[0];
            popupdiv["data-style-top"] = o.config.coords[1];
            popupdiv["data-style-left"] = o.config.coords[0];
            $t._popups[i] = popupdiv;
            $t.element.appendChild(popupdiv);

            var popupheaderdiv = document.createElement("div");
            popupheaderdiv.id = $t.id + "_popups_" + i + "_header";
            var span = document.createElement("span");
            span.className = "popup-header";
            span.innerHTML = o.headline;
            popupheaderdiv.appendChild(span);
            var span = document.createElement("span");
            span.style["float"] = "left";
            span.style.cursor = "pointer";
            span["data-t"] = $t;
            span["data-hsid"] = i;
            span["data-hselid"] = div.id;
            span.innerHTML = "x";
            span.className = "popup-close";
            span.addEventListener("click", function (event) {
                var $t = this["data-t"];
                var i = this["data-hsid"];
                var hselid = this["data-hselid"];
                var hsel = document.getElementById(hselid);

                hsel.style["-ms-transform"] = "rotate(0deg)"; /* IE 9 */
                hsel.style["-webkit-transform"] = "rotate(0deg)"; /* Chrome, Safari, Opera */
                hsel.style["transform"] = "rotate(0deg)";
                hsel["data-direction"] = "off";

                $t._popupHide($t,i);
            });
            popupheaderdiv.appendChild(span);
            var span = document.createElement("span");
            span.className = "clearfix";
            popupheaderdiv.appendChild(span);
            popupdiv.appendChild(popupheaderdiv);

            var popupbodydiv = document.createElement("div");
            popupbodydiv.id = $t.id + "_popups_" + i + "_body";
            popupbodydiv.innerHTML = o.bodyCopy;
            popupdiv.appendChild(popupbodydiv);
        }
    },

    _popupShow : function($t, i){
        // Show overlay
        $t._overlaydiv.style.display = "block";

        var popupdiv = $t._popups[i];
        // Locate popup
        if(document.body.clientWidth < 600) {
            // Place in top
            popupdiv.style.top = "20px";
            popupdiv.style.left = "";
            popupdiv.style.margin = "0 auto";
            popupdiv.style.maxWidth = (document.body.clientWidth - 50)+"px";
        }
        else{
            // In place here
            popupdiv.style.top = popupdiv["data-style-top"];
            popupdiv.style.left = popupdiv["data-style-left"];
            popupdiv.style.margin = "";
            popupdiv.style.maxWidth = "";
        }
        // Show popup
        popupdiv.style.display = "block";
    },

    _popupHide : function($t, i){
        $t._popups[i].style.display = "none";
        $t._overlaydiv.style.display = "none";
    },


    // Image Functions
    _createImage: function ($t) {
        var img = $t.img = document.createElement("img");
        img.id = $t.id + "_img_" + $t._imageFalmilyId;

        if ($t.isInternal) {
            img.src = $t.data.module.hotSpots.image.imageFamily.images.desktop.internalUrl;
            img["data-clear-src"] = $t.data.module.hotSpots.image.imageFamily.images.desktop.internalUrl;
        }
        else {
            img.src = $t.data.module.hotSpots.image.imageFamily.images.desktop.externalUrl;
            img["data-clear-src"] = $t.data.module.hotSpots.image.imageFamily.images.desktop.externalUrl;
        }


        $t.events.elementOnResize(null, $t);

        window.addEventListener("resize", function (event) {
            //debugger;
            //console.log("resized");
            $t.events.elementOnResize(event, $t);
        });

        $t.element.appendChild(img);
    },
    _imageResize: function ($t, width) {
        $t.img.src = $t.img["data-clear-src"] + "&wid=" + width;
        $t.img.width = width;
    },
}

